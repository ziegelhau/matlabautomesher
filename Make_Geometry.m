function [vertex,edges,face,walls,interior] = Make_Geometry...
(firstLength,...
successive,...
coarse_mesh,...
fine_mesh,...
fine_to_coarse,...
coarse_to_fine,...
fileID,...
x_arc_left,...
x_burst_left,...
x_burst_right,...
x_end_left,...
r_cond,...
r_cond_left,...
r_cond_right);

%Edge is a matlab function so we used the variable edges instead

%Creating faces from left to right excluding the spacers and arc since
%their values vary

%Counters
vertex = 0;
edges = 0;
face = 0;
mesh = [];

walls = [];
interior = [];
sym = [];

%-----------------------Most Left Face(Face 1)-----------------------------

edges = edges + Create_Edge(fileID,vertex + 1,vertex + 2);%Edge 1,vertices 1,2
%Here I am creating the mesh for edge 1. Edge 7 will have the same mesh so
%we create a variable to show the relationship.
edge1mesh = coarse_mesh;
%Add the mesh information to an array so we can print it later. We need to
%split the faces with the arc and spacer lines first or else the mesh
%information will be lost
mesh{end + 1} = sprintf(successive,edges,edge1mesh);
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 2,vertex + 3);%Edge 2,vertices 2,3
edge2mesh = coarse_mesh;
mesh{end + 1} = sprintf(successive,edges,edge2mesh);
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 3,vertex + 4);%Edge 3,vertices 3,4
edge3mesh = coarse_mesh;
mesh{end + 1} = sprintf(successive,edges,edge3mesh);
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 4,vertex + 5);%Edge 4,vertices 4,5
edge4mesh = coarse_mesh;
mesh{end + 1} = sprintf(successive,edges,edge4mesh);
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 5,vertex + 6);%Edge 5,vertices 5,6
edge5mesh = coarse_to_fine;
mesh{end + 1} = sprintf(firstLength,edges,edge5mesh);
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 1,vertex + 7);%Edge 6,vertices 1,7
edge6mesh = coarse_mesh;
mesh{end + 1} = sprintf(successive,edges,edge6mesh);
sym = [sym '"edge.',num2str(edges),'" '];

%Update the vertex number after sections of edges have been created so if
%we want to edit this code later and insert some lines, we don't need to
%modify as much of the code
vertex = vertex + 6;

edges = edges + Create_Edge(fileID,vertex + 1,vertex + 2);%Edge 7,vertices 7,8
mesh{end + 1} = sprintf(successive,edges,edge1mesh);      %Same mesh as 1
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 2,vertex + 3);%Edge 8,vertices 8,9
mesh{end + 1} = sprintf(successive,edges,edge2mesh);
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 3,vertex + 4);%Edge 9,vertices 9,10
mesh{end + 1} = sprintf(successive,edges,edge3mesh);
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 4,vertex + 5);%Edge 10,vertices 10,11
mesh{end + 1} = sprintf(successive,edges,edge4mesh);
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 5,vertex + 6);%Edge 11,vertices 11,12
mesh{end + 1} = sprintf(firstLength,edges,edge5mesh);
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex,vertex + 6);    %Edge 12,vertices 6,12
mesh{end + 1} = sprintf(successive,edges,edge6mesh);
walls = [walls '"edge.',num2str(edges),'" '];

vertex = vertex + 6;

face = face + Create_Face(fileID,edges,edges - 1,edges - 2,edges - 3,edges - 4,edges - 5,edges - 6,edges - 7,edges - 8,edges - 9,edges - 10,edges - 11);

%-----------------------Face Under Airbox(Face 5)--------------------------
%I am creating the edges from left to right. We neeed information on one
%edge that is also part of face 4 to create this face. This is why the
%edges for face 3 are created before face 2

edges = edges + Create_Edge(fileID,vertex - 4,vertex + 1);%Edge 13,vertices 8,13
%Mesh Later
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 1,vertex + 3);%Edge 14,vertices 13,15
edge14mesh = fine_mesh;
mesh{end + 1} = sprintf(successive,edges,edge14mesh);
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 3,vertex + 6);%Edge 15,vertices 15,18
%Mesh Later
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex,vertex + 2);    %Edge 16,vertices 12,14
%Mesh Later
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 2,vertex + 4);%Edge 17,vertices 14,16
mesh{end + 1} = sprintf(successive,edges,edge14mesh);
fprintf(fileID,'physics create "burst_disk" btype "WALL" edge "edge.%d" \r\n',edges);

edges = edges + Create_Edge(fileID,vertex + 4,vertex + 10);%Edge 18,vertices 16,22
%Mesh Later
walls = [walls '"edge.',num2str(edges),'" '];

%If the middle conductor is larger than the left one. Vertices 17 and 18
%will be swapped. Then we need to connect 17 and 19 instead
if r_cond > r_cond_left
    edges = edges + Create_Edge(fileID,vertex + 5,vertex + 7);%Edge 19,vertices 17,19
    %Do not mesh in this case
else
    edges = edges + Create_Edge(fileID,vertex + 6,vertex + 7);%Edge 19,vertices 18,19
    mesh{end + 1} = sprintf(successive,edges,edge2mesh);
end
walls = [walls '"edge.',num2str(edges),'" '];         

edges = edges + Create_Edge(fileID,vertex + 7,vertex + 8);    %Edge 20,vertices 19,20
mesh{end + 1} = sprintf(successive,edges,edge3mesh);
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 8,vertex + 9);    %Edge 21,vertices 20,21
mesh{end + 1} = sprintf(successive,edges,edge4mesh);
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 9,vertex + 10);   %Edge 22,vertices 21,22
mesh{end + 1} = sprintf(firstLength,edges,edge5mesh);
walls = [walls '"edge.',num2str(edges),'" '];

vertex = vertex + 11;

%---------------------------Airbox(Face 2,3,4)---------------------------------
edges = edges + Create_Edge(fileID,vertex - 9,vertex);        %Edge 23,vertices 14,23
edge23mesh = fine_mesh;
mesh{end + 1} = sprintf(successive,edges,edge23mesh);
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex,vertex + 1);        %Edge 24,vertices 23,24
mesh{end + 1} = sprintf(successive,edges,edge14mesh);
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex - 7,vertex + 1);    %Edge 25,vertices 16,24
walls = [walls '"edge.',num2str(edges),'" '];
mesh{end + 1} = sprintf(successive,edges,edge23mesh);

airbox  = [];

face = face + Create_Face(fileID,edges,edges - 1,edges - 2,edges - 8); %Face 2
mesh{end + 1} = sprintf('face mesh "face.%d" size %d \r\n',face,fine_mesh);
airbox = [airbox '"face.',num2str(face),'" '];

edges = edges + Create_Edge(fileID,vertex,vertex + 3);       %Edge 26,vertices 23,26
%Mesh...
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 3,vertex + 4);   %Edge 27,vertices 26,27
%Mesh...
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 1,vertex + 4);   %Edge 28,vertices 24,27
%Mesh...
interior = [interior '"edge.',num2str(edges),'" '];

face = face + Create_Face(fileID,edges,edges - 1,edges - 2,edges - 4); %Face 3
airbox = [airbox '"face.',num2str(face),'" '];
mesh{end + 1} = sprintf('face mesh "face.%d" triangle size %d \r\n',face,fine_mesh);

edges = edges + Create_Edge(fileID,vertex + 4,vertex + 5);   %Edge 29,vertices 27,28
%Mesh...
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 2,vertex + 5);   %Edge 30,vertices 25,28
%Mesh...
fprintf(fileID,'physics create "airbox_outlet" btype "WALL" edge "edge.%d" \r\n',edges);

edges = edges + Create_Edge(fileID,vertex + 1,vertex + 2);   %Edge 31,vertices 24,25
%Mesh...
walls = [walls '"edge.',num2str(edges),'" '];

face = face + Create_Face(fileID,edges,edges - 1,edges - 2, edges - 3); %Face 4
mesh{end + 1} = sprintf('face mesh "face.%d" size %d \r\n',face,fine_mesh);
airbox = [airbox '"face.',num2str(face),'" '];

fprintf(fileID,'physics create "airbox" ctype "FLUID" face %s \r\n',airbox);

vertex = vertex +  6;

%There are 31 edges that will always be created when creating the edges
%from left to right I am going to continue labelling the edges as if every
%one was created.

%It is possible that vertices 17 and 18 made be coincident. To account for
%this we create a variable extra_vertex1. If 17 and 18 are coincident it
%will have a value of 1 so vertex 17 is not used. If 17 is used, it will
%have a value of 0.

if r_cond == r_cond_left
    extra_vertex1 = 1;    
    face = face + Create_Face(fileID,edges - 9,edges - 10,edges - 11,edges - 12,edges - 13,edges - 14,edges - 15,edges - 16,edges - 17,edges - 18,edges - 20,edges - 21, edges - 22,edges - 23);
elseif r_cond > r_cond_left
    extra_vertex1 = 0;
    edges = edges + Create_Edge(fileID,vertex - 11,vertex - 12);%Edge 32,Vertices 17,18
    %Don't mesh
    walls = [walls '"edge.',num2str(edges),'" '];        
    face = face + Create_Face(fileID,edges,edges - 10,edges - 11,edges - 12,edges - 13,edges - 14,edges - 15,edges - 16,edges - 17,edges - 18,edges - 19,edges - 21, edges - 22,edges - 23,edges - 24);
else
    extra_vertex1 = 0;
    edges = edges + Create_Edge(fileID,vertex - 11,vertex - 12);%Edge 32,Vertices 17,18    
    face = face + Create_Face(fileID,edges - 10,edges - 11,edges - 12,edges - 13,edges - 14,edges - 15,edges - 16,edges - 17,edges - 18,edges - 19,edges - 21, edges - 22,edges - 23,edges - 24);
    mesh{end + 1} = sprintf(successive,edges,coarse_mesh);
    walls = [walls '"edge.',num2str(edges),'" '];
end

%-------------------------Middle Face(Face 6)------------------------------
if r_cond == r_cond_right
    extra_vertex2 = 1;
else
    extra_vertex2 = 0;
end

edges = edges + Create_Edge(fileID,vertex - 12 + extra_vertex1,vertex + extra_vertex2);%Edge 33,Vertices 17 or 18,29 or 30
edge27mesh = coarse_mesh;
mesh{end + 1} = sprintf(successive,edges,edge27mesh);
walls = [walls '"edge.',num2str(edges),'" '];

%This could be in the previous if statement but I thought it was clearer if
%it was another if statement
if r_cond ~= r_cond_right
    edges = edges + Create_Edge(fileID,vertex,vertex + 1);      %Edge 34,Vertices 29,30
    %Don't mesh
    walls = [walls '"edge.',num2str(edges),'" '];
end

%If the middle conductor is larger than the right conductor, vertices 29
%and 30 will be switched.
if r_cond > r_cond_right
    edges = edges + Create_Edge(fileID,vertex,vertex + 2);      %Edge 35,Vertices 29,31
else
    edges = edges + Create_Edge(fileID,vertex + 1,vertex + 2);  %Edge 35,Vertices 30,31
end
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 2,vertex + 3);      %Edge 36,Vertices 31,32
%Don't mesh
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 3,vertex + 4);      %Edge 37,Vertices 32,33
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex - 8,vertex + 4);      %Edge 38,Vertices 33,21
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

vertex = vertex + 4;

%The middle face will consist of a different number of edges depending on
%the radius of the left, middle, and right conductor
%here are the possibilites with varying heights of the left and right
%conductors relative to the middle conductor

%possibilites		               Edges									
%left	 right										
%lower	 lower	  38	37	36	35		33		21	20	19
%lower	 equal		    37	36	35	34	33		21	20	19
%lower	 greater  38	37	36	35	34	33		21	20	19
%equal	 lower	    	37	36	35	34		32	21	20	19
%equal	 equal			    36	35	34	33	32	21	20	19
%equal	 greater		37	36	35	34	33	32	21	20	19
%greater lower	  38	37	36	35		33	32	21	20	19
%greater equal	    	37	36	35	34	33	32	21	20	19
%greater greater  38	37	36	35	34	33	32	21	20	19

%I will create the if statements in this order

if r_cond_left < r_cond
    if r_cond_right < r_cond
        fprintf(fileID,'face create wireframe "edge.38" "edge.37" "edge.36" "edge.35"           "edge.33"           "edge.21" "edge.20" "edge.19" \r\n');
    elseif r_cond_right == r_cond
        fprintf(fileID,'face create wireframe           "edge.37" "edge.36" "edge.35" "edge.34" "edge.33"           "edge.21" "edge.20" "edge.19" \r\n');
    else
        fprintf(fileID,'face create wireframe "edge.38" "edge.37" "edge.36" "edge.35" "edge.34" "edge.33"           "edge.21" "edge.20" "edge.19" \r\n');
    end
elseif r_cond_left == r_cond
    if r_cond_right < r_cond
        fprintf(fileID,'face create wireframe           "edge.37" "edge.36" "edge.35" "edge.34" "edge.33" "edge.32" "edge.21" "edge.20" "edge.19" \r\n');
    elseif r_cond_right == r_cond
        fprintf(fileID,'face create wireframe                     "edge.36" "edge.35" "edge.34" "edge.33" "edge.32" "edge.21" "edge.20" "edge.19" \r\n');
    else
        fprintf(fileID,'face create wireframe           "edge.37" "edge.36" "edge.35" "edge.34" "edge.33" "edge.32" "edge.21" "edge.20" "edge.19" \r\n');
    end
else
    if r_cond_right < r_cond
        fprintf(fileID,'face create wireframe "edge.38" "edge.37" "edge.36" "edge.35"           "edge.33" "edge.32" "edge.21" "edge.20" "edge.19" \r\n');
    elseif r_cond_right == r_cond
        fprintf(fileID,'face create wireframe           "edge.37" "edge.36" "edge.35" "edge.34" "edge.33" "edge.32" "edge.21" "edge.20" "edge.19" \r\n');
    else
        fprintf(fileID,'face create wireframe "edge.38" "edge.37" "edge.36" "edge.35" "edge.34" "edge.33" "edge.32" "edge.21" "edge.20" "edge.19" \r\n');
    end
end
face = face + 1;

%-----------------------Middle Right Face(Face 5)--------------------------
edges = edges + Create_Edge(fileID,vertex,vertex + 1);      %Edge 39,Vertices 33,34
edge39mesh = coarse_mesh;
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex - 3,vertex + 3);  %Edge 40,Vertices 30,36
edge40mesh = coarse_mesh;
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 3,vertex + 4);  %Edge 41,Vertices 36,37
%Don't mesh
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 4,vertex + 5);  %Edge 42,Vertices 37,38
%Don't mesh
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 5,vertex + 6);  %Edge 43,Vertices 38,39
%Don't mesh
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 6,vertex + 7);  %Edge 44,Vertices 39,40
%Don't mesh
interior = [interior '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 1,vertex + 7);  %Edge 45,Vertices 34,40
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

vertex = vertex + 8;

%If the middle conductor is larger than the right conductor we need to
%include edge 34 in in thi middle right face
if r_cond > r_cond_right
    face = face + Create_Face(fileID,edges,edges - 1,edges - 2,edges - 3,edges - 4,edges - 5,edges - 6,edges - 8,edges - 9,edges - 10,edges - 11);
else
    face = face + Create_Face(fileID,edges,edges - 1,edges - 2,edges - 3,edges - 4,edges - 5,edges - 6,edges - 8,edges - 9,edges - 10);
end

%----------------------------Right Face(Face 6)----------------------------
edges = edges + Create_Edge(fileID,vertex - 5,vertex - 6);  %Edge 46,Vertices 35,36
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex - 6,vertex);      %Edge 47,Vertices 35,41
%Don't mesh
sym = [sym '"edge.',num2str(edges),'" '];
fprintf(fileID,'physics create "sym" btype "AXIS" edge %s \r\n',sym);

edges = edges + Create_Edge(fileID,vertex,vertex + 1);      %Edge 48,Vertices 41,42
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 1,vertex + 2);  %Edge 49,Vertices 42,43
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 2,vertex + 3);  %Edge 50,Vertices 43,44
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 3,vertex + 4);  %Edge 51,Vertices 44,45
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex + 4,vertex + 5);  %Edge 52,Vertices 45,46
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

edges = edges + Create_Edge(fileID,vertex - 1,vertex + 5);  %Edge 53,Vertices 40,46
%Don't mesh
walls = [walls '"edge.',num2str(edges),'" '];

vertex = vertex + 6;

face = face + Create_Face(fileID,edges,edges - 1,edges - 2,edges - 3,edges - 4,edges - 5,edges - 6,edges - 7,edges - 9,edges - 10,edges - 11,edges - 12);

%Mesh all edges and airbox face from left to right
for i=1:length(mesh)
    fprintf(fileID, mesh{i});
end