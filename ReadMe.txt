Code written in 2015 while working at ABB

How to use the automeshing matlab script

1. Check that the file IAT_HVGIS_parameters_*** is in this directory.
2. Open the A_Main.m file and enter the file name(IAT_HVGIS_parameters_538) on line 57
3. Save and run the Automated_Mesh_script.m script
4. A journal file will be created in the same directory as A_Main.m called JOURNAL.jou
5. Open gambit and run the journal (File > Run Journal > Browse > JOURNAL.jou)
6. A mesh will be created and saved in the directory in which you opened Gambit named "file name".msh (IAT_HVGIS_parameters_538.msh)
7. The matlab file will ouput the arc_volume to the screen. When running the simulation the arc volume in the current_power.c needs to be changed
   to this value