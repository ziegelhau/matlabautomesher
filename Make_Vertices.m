function Make_Vertices...
(x_end_left,...
x_start_right,...
r_cond_left,...
y_left1,...
y_left2,...
r_enclosure,...
r_left,...
r_cond_right,...
y_right1,...
y_right2,...
r_right,...
r_cond,...
fileID,...
create_vertex,...
x_start_cond,...
x_burst_left,...
x_burst_right,...
x_burst,...
x_end_cond,...
x_end_right)

origin = 0;

x_values = [origin, x_start_cond];
y_values = [origin r_cond_left y_left1 y_left2 r_enclosure r_left];
for i = 1:2
    for j = 1:6
        fprintf(fileID, create_vertex, x_values(i), y_values(j)); % vertices 1 to 12
    end
end

x_values = [x_burst_left x_burst_right];
y_values = [r_cond_left r_left];
for i = 1:2
    for j = 1:2
        fprintf(fileID, create_vertex, x_values(i), y_values(j)); % vertices 13 to 16
    end
end

y_values = [r_cond r_cond_left y_left1 y_left2 r_enclosure r_left];
for i = 1:6
    fprintf(fileID, create_vertex, x_end_left, y_values(i)); % vertices 17 to 22
end

r_inner = r_left + 0.03/(2 * pi * r_left);
outlet_diameter = 0.12; %In 3D
%Maths
%Area 2D = Area 3D
%pi(r_outer ^ 2 - r_inner ^ 2) = pi(outlet_diameter ^ 2/4) Rearrange this
r_outer = sqrt(outlet_diameter ^ 2 / 4 + r_inner ^ 2);
%Not sure what this should be reduced by
x_outlet = x_burst_right + 0.1 / (2 * pi * r_left);

fprintf(fileID, create_vertex, x_burst_left ,r_inner); % vertice 23
fprintf(fileID, create_vertex, x_burst_right,r_inner); % vertice 24
fprintf(fileID, create_vertex, x_outlet     ,r_inner); % vertice 25
fprintf(fileID, create_vertex, x_burst      ,r_outer); % vertice 26
fprintf(fileID, create_vertex, x_burst_right,r_outer); % vertice 27
fprintf(fileID, create_vertex, x_outlet     ,r_outer); % vertice 28

y_values = [r_cond r_cond_right y_right1 y_right2 r_enclosure r_right];
for i = 1:6
    fprintf(fileID, create_vertex, x_start_right, y_values(i));   % vertices 29 to 33
end

x_values = [x_end_cond x_end_right];
y_values = [origin r_cond_right y_right1 y_right2 r_enclosure r_right];

for i = 1:2
    for j = 1:6
        fprintf(fileID, create_vertex, x_values(i), y_values(j)); % vertices 34 to 45
    end
end