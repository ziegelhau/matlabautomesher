function Boundary_Conditions(fileID,interior,walls,face)

%Create Remaining Boundaries
fprintf(fileID,'physics create "interior" btype "INTERIOR" edge %s \r\n ',interior);
fprintf(fileID,'physics create "wall" btype "WALL" edge %s \r\n ',walls);

%We can't know which faces are fluid in advance. To solve this we can try
%and make every face a fluid. Since the airbox and arc have already been
%assigned, gambit won't change their continuum type to fluid.
fluid = [];

for i = 1:face
    fluid = [fluid '"face.',num2str(i),'" '];
end
fprintf(fileID,' physics create "fluid" ctype "FLUID" face %s \r\n ',fluid);
