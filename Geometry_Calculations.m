function [x_arc_left,...
x_arc_right,...
y_left1,...
y_left2,...
y_right1,...
y_right2,...
y_spacer_vertex1,...
y_spacer_vertex2,...
burst_width,...
x_burst_left,...
x_burst_right,...
x_spacer,...
A_spacer]...
= Geometry_Calculations ...    
(r_right,...
x_end_cond,...
initial_arc_position,...
r_enclosure,...
r_cond_left,...
A_left,...
r_cond_right,...
A_right,...
r_left,...
A_burst,...
x_burst,...
N_spacers,...
r_cond,...
x_end_left,...
x_start_right,...
values)

arc_width = 0.04; %why is it 0.04?

%Calculating the arc volume, need to put this value in current_power.c
if initial_arc_position < x_end_left    
    arc_volume = pi * (r_left ^ 2 - r_cond_left ^ 2) * arc_width;    
elseif initial_arc_position > x_start_right
    arc_volume = pi * (r_right ^ 2 - r_cond_right ^ 2) * arc_width;    
else
    arc_volume = pi * (r_enclosure ^ 2 - r_cond ^ 2) * arc_width;
end
fprintf('The arc volume is %d, this value needs to be put in current_power.c \r\n',arc_volume);

%calculate x values of arc zone:
x_arc_left  = initial_arc_position - arc_width / 2;
x_arc_right = initial_arc_position + arc_width / 2;

%calculate y value of inner vertices of walls and spacers:
%%wall left
p = (r_enclosure ^ 2 - r_cond_left ^ 2 - A_left / pi) / (2 * (r_enclosure + r_cond_left));

y_left1 = r_cond_left + p;
y_left2 = r_enclosure - p;

%%wall right
r = (r_enclosure ^ 2 - r_cond_right ^ 2 - A_right / pi) / (2 * (r_enclosure + r_cond_right));
y_right1 = r_cond_right + r;
y_right2 = r_enclosure - r;

%create a vector containing all the x_spacers and A_spacers in numerical
%order with the size of N_spacers
%N_spacers is the 16th variable that will be created
if N_spacers ~= 0
    A_spacer = zeros(1,N_spacers);
    x_spacer = zeros(1,N_spacers);
    
    %This loop relies on the position of x_spacer and A_Spacer in the
    %IAT_HVGIS file. The value of x_spacer is stored in the 17th variable
    %in the sheet. It then occurs ever other time. To find and store that variable
    %in the correct name, we do the following.
    for i = 1:N_spacers
        j = i + 16;
        x_spacer(i) = values((j - 17) * 2 + 17);
        A_spacer(i) = values((j - 17) * 2 + 18);
    end
end

%%spacer wall
if N_spacers ~= 0 %if there are no spacers, it doesn't calculate y values
    %%sort spacers from left to right i.e. the left most spacer is spacer 1
     [x_spacer,indices] = sort(x_spacer);
    A_spacer = A_spacer(indices);
    
    [x_spacer,indices] = sort(x_spacer);
    A_spacer = A_spacer(indices);
        
    %Math to calculate spacer vertices proof with r1 and r2 being the
    %vertices
    %A2D = A3D
    %pi(r2 ^ 2 - r1 ^ 2) = A3D
    %If we put the spacer exactly in the middle, we can say that there is 
    %some distance deltaR, away from r_enclosure and r_cond where the
    %vertices need to go.
    %pi((r_enclosure - deltaR)^2 - (r_inner + deltaR)^2) = A3D
    %renclosure^2 -
    %2*r_enclosure*deltaR+deltaR^2-(r_cond^2+2*r_cond*deltaR+deltaR^2)=A3D/pi
    %deltaR = (r_enclosure ^ 2 - r_cond ^ 2 - A_spacer(i)/ pi ) / (2 * (r_enclosure + r_cond))
    
    %calculates y values of spacers and puts them into a vector
    for i = 1:N_spacers
        deltaR = (r_enclosure ^ 2 - r_cond ^ 2 - A_spacer(i)/ pi ) / (2 * (r_enclosure + r_cond));
        y_spacer_vertex1(i) = r_cond + deltaR;
        y_spacer_vertex2(i) = r_enclosure - deltaR;
    end
else
    %Just assigning a value so it can be outputted and code doesn't crash
    y_spacer_vertex1 = 0;
    y_spacer_vertex2 = 0;
    x_spacer = 0;
    A_spacer = 0;
end

%calculate burst disk width and x values of vertices:
%Maths
%Area 2D = Area 3D
%2 * pi * r_left * burst_width = A_burst Rearrange this
burst_width = A_burst / (2 * pi * r_left); %burst disk width
x_burst_left = x_burst - burst_width / 2; %left vertex of burst disk
x_burst_right = x_burst + burst_width / 2; %right vertex of burst disk
