function [walls,face] = Arc_And_Spacer...
(x_end_cond,...
x_start_cond,...
x_burst_left,...
x_burst_right,...
initial_arc_position,...
x_end_left,...
x_start_right,...
r_cond_left,...
y_left1,...
y_left2,...
r_enclosure,...
r_left,...
r_cond_right,...
y_right1,...
y_right2,...
r_right,...
N_spacers,...
x_spacer,...
x_arc_left,...
x_arc_right,...
r_cond,...
y_spacer_vertex1,...
y_spacer_vertex2,...
fileID,...
create_vertex,...
edges,vertex,...
walls,...
face,...
mesh_order,...
firstLength,...
successive,...
fine_to_medium,...
medium_to_fine,...
fine_to_coarse,...
coarse_to_fine,...
medium_to_coarse,...
coarse_to_medium,...
coarse_mesh,...
medium_mesh,...
fine_mesh)
%This function creates the arc and spacers
%Most of the checks here are just for when the arc is on the left face
mesh = [];

if initial_arc_position < x_end_left || initial_arc_position > x_start_right
    if initial_arc_position < x_end_left
        y_values = [r_cond_left y_left1 y_left2 r_enclosure  r_left];
        face_to_split  = 5;
        left_vertex    = x_start_cond;
        right_vertex   = x_end_left;
    else
        y_values = [r_cond_right y_right1 y_right2 r_enclosure r_right];
        face_to_split  = 7;
        left_vertex    = x_start_right;
        right_vertex   = x_end_cond;
    end
    x_values = [x_arc_left x_arc_right];
    
    %print arc zone vertices
    for i = 1:length(x_values)
        for j = 1:length(y_values)
            fprintf(fileID, create_vertex, x_values(i), y_values(j)); % vertices 47 to 55
        end
    end    
    
    %Create arc edges
    %We do not want to mesh the bottom vertical lines of the arc edges
    %because if the middle conductor is larger than the left or right one,
    %the mesh elements may not line up. Only mesh them if they are equal to
    %the middle conductor
    edges = edges + Create_Edge(fileID,vertex    ,vertex + 1); %Edge 54,Vertices 47,48
    %Don't mesh
    
    edges = edges + Create_Edge(fileID,vertex + 1,vertex + 2); %Edge 55,Vertices 48,49
    %Don't mesh

    edges = edges + Create_Edge(fileID,vertex + 2,vertex + 3); %Edge 56,Vertices 49,50
    
    edges = edges + Create_Edge(fileID,vertex + 3,vertex + 4); %Edge 57,Vertices 50,51
    if initial_arc_position < x_end_left
        mesh{end + 1} = sprintf(firstLength,edges,fine_to_coarse);
    end
       
    fprintf(fileID, 'face split "face.%d" edges "edge.%d" "edge.%d" "edge.%d" "edge.%d" \r\n',face_to_split,edges,edges - 1,edges - 2,edges - 3);  % splits face with left arc zone wall
    face = face + 1;
    %When a face is split, the original face becomes the one with the
    %larger area. We need to know which face is where so we can split the
    %correct one        
    
    if  right_vertex - x_arc_left < (right_vertex - left_vertex)/2
        face_elem = find(mesh_order(1,:) == face_to_split);
        mesh_order(2,face_elem) = left_vertex;
        temp = [face;x_arc_left];
        mesh_order = [mesh_order temp];
        face_to_split = face;
    else
        face_elem = find(mesh_order(1,:) == face_to_split);
        mesh_order(2,face_elem) = x_arc_left;
        temp = [face;left_vertex];
        mesh_order = [mesh_order temp];
    end
    
    %The next line that is going to split the face is to the right of the
    %last line so the left vertex needs to be x_arc_left
    left_vertex = x_arc_left ;
    
    walls = [walls '"edge.',num2str(edges + 1),'" '];
    walls = [walls '"edge.',num2str(edges + 6),'" '];
    
    vertex = vertex + 5;
    %When a face is split, 2 new edges are created. But gambit skips
    %numbering 4 edges for some reason(on this face). That's why it is plus 6
    edges = edges + 6;
    
    edges = edges + Create_Edge(fileID,vertex,vertex + 1);    %Edge 64,Vertices 54,55
    %Don't mesh
    
    edges = edges + Create_Edge(fileID,vertex + 1,vertex + 2);%Edge 65,Vertices 55,56
    %Don't mesh
    
    edges = edges + Create_Edge(fileID,vertex + 2,vertex + 3);%Edge 66,Vertices 56,57
    %Don't mesh
    
    edges = edges + Create_Edge(fileID,vertex + 3,vertex + 4);%Edge 67,Vertices 57,58
    if initial_arc_position < x_end_left
        mesh{end + 1} = sprintf(firstLength,edges,fine_to_coarse);
    end
    
    fprintf(fileID, 'face split "face.%d" edges "edge.%d" "edge.%d" "edge.%d" "edge.%d" \r\n',face_to_split,edges,edges - 1,edges - 2,edges - 3);  % splits face with right arc zone wall
    face = face + 1;
    %If the arc area is larger than the face that was just split it will
    %change what face number the arc is
    if  right_vertex - x_arc_right < (right_vertex - left_vertex)/2
        face_elem = find(mesh_order(1,:) == face_to_split);
        mesh_order(2,face_elem) = left_vertex;
        temp = [face;x_arc_right];
        mesh_order = [mesh_order temp];
        arc_face = face - 1;
    else
        face_elem = find(mesh_order(1,:) == face_to_split);
        mesh_order(2,face_elem) = x_arc_right;
        temp = [face;left_vertex];
        mesh_order = [mesh_order temp];
        arc_face = face;
    end
    
    fprintf(fileID,' physics create "arc" ctype "FLUID" face "face.%d" \r\n ',arc_face);
    
    %-----------------------------Mesh Arc Faces---------------------------
    %This will only happen when the arc is on the left face If the arc is
    %close to the burst disk, the mesh doesn't have time to grow smoothly.
    %In this case we will make the arc use a mesh of first length. 10
    %elements is enough elements to grow smoothly. 10 elements will occur
    %when the distance between two vertices is the average of the two mesh
    %sizes times 10 elements,
    coarse_10 = (coarse_mesh + fine_mesh) / 2 * 10;
    medium_10 = (medium_mesh + fine_mesh) / 2 * 10;
    
    %If face 3 is very short it might screw up
    %Check if arc is on the left face
    if x_arc_left < x_end_left
        %If entire arc is on the most left line
        if x_burst_left > x_arc_right
            %If left arc line is on left half of face
            if x_burst_left - x_arc_left > (x_burst_left - x_start_cond)/2
                mesh{end + 1} = sprintf(successive,edges - 9,coarse_mesh);
                mesh{end + 1} = sprintf(successive,edges - 4,coarse_mesh);
                %Assuming that the face is large enough that the right arc
                %line won't ever be on the right half of the new face
                mesh{end + 1} = sprintf(successive,edges + 1,coarse_mesh);
                mesh{end + 1} = sprintf(successive,edges + 6,coarse_mesh);
                %This needs to be an else if and we need to check if it's still
                %on the left side of the burst disk
            else
                %If right arc can make 10 elements transitioning from the
                %fine mesh to the coarse mesh
                if x_burst_left - x_arc_right > coarse_10
                    mesh{end + 1} = sprintf(successive,edges  - 9,coarse_mesh);
                    mesh{end + 1} = sprintf(successive,edges  - 4,coarse_mesh);
                    mesh{end + 1} = sprintf(firstLength,edges + 1,coarse_to_fine);
                    mesh{end + 1} = sprintf(firstLength,edges + 6,coarse_to_fine);
                    %If right arc can't make 10 elements transitioning from the
                    %fine mesh to the coarse mesh
                else
                    %If right arc can make 10 elements transitioning from the
                    %fine mesh to the medium mesh
                    if x_burst_left - x_arc_right > medium_10
                        mesh{end + 1} = sprintf(firstLength,edges - 9,coarse_to_medium);
                        mesh{end + 1} = sprintf(firstLength,edges - 4,coarse_to_medium);
                        mesh{end + 1} = sprintf(firstLength,edges + 1,medium_to_fine);
                        mesh{end + 1} = sprintf(firstLength,edges + 6,medium_to_fine);
                        %If right arc can't make 10 elements transitioning from the
                        %fine mesh to the medium mesh
                    else
                        mesh{end + 1} = sprintf(firstLength,edges - 9,medium_to_fine);
                        mesh{end + 1} = sprintf(firstLength,edges - 4,medium_to_fine);
                        mesh{end + 1} = sprintf(successive,edges  + 1,fine_mesh);
                        mesh{end + 1} = sprintf(successive,edges  + 6,fine_mesh);
                    end
                end
            end
            %If entire arc is on right side of the face
        elseif x_burst_right < x_arc_left
            %If left arc line is on right half of face
            if x_arc_left - x_burst_right > (x_end_left - x_burst_right)/2
                mesh{end + 1} = sprintf(successive,edges - 9,coarse_mesh);
                mesh{end + 1} = sprintf(successive,edges - 4,coarse_mesh);
                %Assuming that the face is large enough that the right arc
                %line won't ever be on the right half of the new face
                mesh{end + 1} = sprintf(successive,edges + 1,coarse_mesh);
                mesh{end + 1} = sprintf(successive,edges + 6,coarse_mesh);
            else
                %If right arc can make 10 elements transitioning from the
                %fine mesh to the coarse mesh
                if x_arc_left - x_burst_right > coarse_10
                    mesh{end + 1} = sprintf(firstLength,edges - 9,fine_to_coarse);
                    mesh{end + 1} = sprintf(firstLength,edges - 4,fine_to_coarse);
                    mesh{end + 1} = sprintf(successive,edges  + 1,coarse_mesh);
                    mesh{end + 1} = sprintf(successive,edges  + 6,coarse_mesh);
                    %If right arc can't make 10 elements transitioning from the
                    %fine mesh to the coarse mesh
                else
                    %If right arc can make 10 elements transitioning from the
                    %fine mesh to the medium mesh
                    if x_arc_left - x_burst_right > medium_10
                        mesh{end + 1} = sprintf(firstLength,edges - 9,fine_to_medium);
                        mesh{end + 1} = sprintf(firstLength,edges - 4,fine_to_medium);
                        mesh{end + 1} = sprintf(firstLength,edges + 1,medium_to_coarse);
                        mesh{end + 1} = sprintf(firstLength,edges + 6,medium_to_coarse);
                        %If right arc can't make 10 elements transitioning from the
                        %fine mesh to the medium mesh
                    else
                        mesh{end + 1} = sprintf(successive,edges  - 9,fine_mesh);
                        mesh{end + 1} = sprintf(successive,edges  - 4,fine_mesh);
                        mesh{end + 1} = sprintf(firstLength,edges + 1,fine_to_medium);
                        mesh{end + 1} = sprintf(firstLength,edges + 6,fine_to_medium);
                    end
                end
            end
        else
            %Here goes when one line is under burst disk or the lines are
            %on both sides
        end        
    end
    
    %When the arc splits a line it loses its meshing instructions. We need
    %to mesh lines 13,15,16,18 here then    
    
    edge13mesh = coarse_to_fine;
%When a  face is split by a line, the original edge ends up as the longer
%edge. So we have to check which side it ends up on if it gets split and
%check again if it gets split by the right arc line
if x_arc_left < x_burst_left
    if x_burst_left - x_arc_left < (x_burst_left - x_start_cond)/2
        mesh{end + 1} = sprintf(successive,13,coarse_mesh);
        mesh{end + 1} = sprintf(successive,16,coarse_mesh);
    else
        %Check that the right arc edge is left of the left burst
        %disk edge
        if x_burst_left - x_arc_right > 0 && x_burst_left - x_arc_right < (x_burst_left - x_arc_left)/2
            mesh{end + 1} = sprintf(successive,13,coarse_mesh);
            mesh{end + 1} = sprintf(successive,16,coarse_mesh);
        else
            mesh{end + 1} = sprintf(firstLength,13,edge13mesh);
            mesh{end + 1} = sprintf(firstLength,16,edge13mesh);
        end
    end
else
    mesh{end + 1} = sprintf(firstLength,13,edge13mesh);
    mesh{end + 1} = sprintf(firstLength,16,edge13mesh);
end

%If this case is true, the arc will split lines 15 and 18. If this happens
%we can use a coarse mesh to mesh these edges instead
if x_arc_left < x_end_left
    if x_arc_left - x_burst_right > (x_end_left - x_burst_right)/2
        edge15mesh = fine_to_coarse;
        mesh{end + 1} = sprintf(firstLength,15,edge15mesh);
        mesh{end + 1} = sprintf(firstLength,18,edge15mesh);
    elseif x_arc_left - x_burst_right < (x_end_left - x_burst_right)/2 && x_arc_left - x_burst_right > 0
        edge15mesh = coarse_mesh;
        mesh{end + 1} = sprintf(successive,15,edge15mesh);
        mesh{end + 1} = sprintf(successive,18,edge15mesh);
    else        
        edge15mesh = fine_to_coarse;
        mesh{end + 1} = sprintf(firstLength,15,edge15mesh);
        mesh{end + 1} = sprintf(firstLength,18,edge15mesh);
    end
else
    edge15mesh = fine_to_coarse;
    mesh{end + 1} = sprintf(firstLength,15,edge15mesh);
    mesh{end + 1} = sprintf(firstLength,18,edge15mesh);
end 

    %When you split a face new vertices are created.(5 for this face) We
    %need to add 10 because the face is split twice. adding 10 vertices has
    %to be done after the edges are created because when we create the
    %edges we use the old vertices.
    vertex = vertex + 10;
    
    walls = [walls '"edge.',num2str(edges + 1),'" '];
    walls = [walls '"edge.',num2str(edges + 6),'" '];
    
    vertex = vertex + 5;
    edges = edges + 6;
    
    if N_spacers ~= 0
        left_vertex  = x_end_left;
        right_vertex = x_start_right;
        face_to_split = 4;
        for i = 1:N_spacers
            y_spacer = [r_cond y_spacer_vertex1(i) y_spacer_vertex2(i) r_enclosure];
            for j = 1:4
                %create spacer vertices
                fprintf(fileID, create_vertex, x_spacer(i), y_spacer(j));
            end
            %Create Edge
            edges  = edges + Create_Edge(fileID,vertex,vertex + 1,vertex + 2,vertex + 3);
            vertex = vertex + 4;
            walls  = [walls '"edge.',num2str(edges),'" "edge.',num2str(edges - 2),'"'];
            
            %Create Face
            fprintf(fileID, 'face split "face.%d" edges "edge.%d" "edge.%d" "edge.%d" \r\n',face_to_split,edges, edges - 1, edges - 2);
            face = face + 1;
            if  right_vertex - x_spacer(i) < (right_vertex - left_vertex)/2
                face_elem = find(mesh_order(1,:) == face_to_split);
                mesh_order(2,face_elem) = left_vertex;
                mesh_order(2,face_to_elem) = left_vertex;
                temp = [face;x_spacer(i)];
                mesh_order = [mesh_order temp];
                face_to_split = face;
            else
                face_elem = find(mesh_order(1,:) == face_to_split);
                mesh_order(2,face_to_elem) = x_spacer(i);
                temp = [face;left_vertex];
                mesh_order = [mesh_order temp];
            end
            left_vertex = x_spacer(i);
            walls  = [walls '"edge.',num2str(edges + 1),'" "edge.',num2str(edges + 5),'"'];
            vertex = vertex + 4;
            edges  = edges + 5;
        end
    end
else
    %If the arc isn't on the left face, mesh the edges with coarse to fine
    edge13mesh = coarse_to_fine;
    mesh{end + 1} = sprintf(firstLength,13,edge13mesh);
    mesh{end + 1} = sprintf(firstLength,16,edge13mesh);
    edge15mesh = fine_to_coarse;
    mesh{end + 1} = sprintf(firstLength,15,edge15mesh);
    mesh{end + 1} = sprintf(firstLength,18,edge15mesh);
   left_vertex   = x_end_left;
    right_vertex  = x_start_right;
    face_to_split = 6;
    
    if N_spacers == 0
        %create arc zone vertices
        x_values = [x_arc_left x_arc_right];
        y_values = [r_cond y_right1 y_right2 r_enclosure];
        length_arc_centre = y_right2 - y_right1; %length of the space between the inner vertices of the arc walls. this is important for meshing
        %print arc zone vertices
        for i = 1:length(x_values)
            for j = 1:length(y_values)
                fprintf(fileID, create_vertex, x_values(i), y_values(j));
            end
        end
        
        %Create arc edges
        edges = edges + Create_Edge(fileID,vertex    ,vertex + 1);%Edge 54,Vertices 47,48
        edges = edges + Create_Edge(fileID,vertex + 1,vertex + 2);%Edge 55,Vertices 48,49
        edges = edges + Create_Edge(fileID,vertex + 2,vertex + 3);%Edge 56,Vertices 49,50
        
        %Create Face
        fprintf(fileID, 'face split "face.%d" edges "edge.%d" "edge.%d" "edge.%d" \r\n',face_to_split,edges, edges - 1, edges - 2);
        face = face + 1;
        if  right_vertex - x_arc_left < (right_vertex - left_vertex)/2
            face_elem = find(mesh_order(1,:) == face_to_split);
            mesh_order(2,face_elem) = left_vertex;
            temp = [face;x_arc_left];
            mesh_order = [mesh_order temp];
            face_to_split = face;                                    
        else
            face_elem = find(mesh_order(1,:) == face_to_split);
            mesh_order(2,face_elem) = x_arc_left;
            temp = [face;left_vertex];
            mesh_order = [mesh_order temp];
        end
        left_vertex = x_arc_left;
        walls = [walls '"edge.',num2str(edges + 1),'" '];
        walls = [walls '"edge.',num2str(edges + 5),'" '];
        
        vertex = vertex + 4;
        %When a face is split, 2 new edges are created. But gambit skips
        %numbering 3 edges for some reason(on this face). That's why it is
        %plus 5
        edges = edges + 5;
        
        edges = edges + Create_Edge(fileID,vertex,vertex + 1);    %Edge 61,Vertices 55,56
        edges = edges + Create_Edge(fileID,vertex + 1,vertex + 2);%Edge 62,Vertices 56,57
        edges = edges + Create_Edge(fileID,vertex + 2,vertex + 3);%Edge 63,Vertices 57,58
        
        %Create Face
        fprintf(fileID, 'face split "face.%d" edges "edge.%d" "edge.%d" "edge.%d" \r\n',face_to_split,edges, edges - 1, edges - 2); % splits face with right arc zone wall
        face = face + 1;
        %If the arc area is larger than the face that was just split it
        %will change what face number the arc is
        if  right_vertex - x_arc_right < (right_vertex - left_vertex)/2
            face_elem = find(mesh_order(1,:) == face_to_split);
            mesh_order(2,face_elem) = left_vertex;
            temp = [face;x_arc_right];
            mesh_order = [mesh_order temp];
            arc_face = face - 1;
        else
            face_elem = find(mesh_order(1,:) == face_to_split);
            mesh_order(2,face_elem) = x_arc_right;
            temp = [face;left_vertex];
            mesh_order = [mesh_order temp];
            arc_face = face;
        end
        
        walls = [walls '"edge.',num2str(edges + 1),'" '];
        walls = [walls '"edge.',num2str(edges + 5),'" '];
        
        vertex = vertex + 4;
        edges  = edges + 5;
        face   = face + 1;
    else        
        %arczone vertices
        %The following statement finds the spacer which is closest to the
        %arc zone so that the arc walls can use the inner vertices of that
        %spacer. Makes the mesh nicer since the vertices connect.
        
        %Is this important? Do we need to do this? Or can we just make the arc
        %have two lines? I think we could just make the arc have two lines        
        smallest_dist = inf;
        for i = 1:N_spacers
            if abs(initial_arc_position - x_spacer(i)) < smallest_dist
                smallest_dist = abs(initial_arc_position - x_spacer(i));
                spacer_number = i;
            end
        end
        
        index_counter = 1;
        for i = 1:N_spacers
            %Do this until we get to the arc(x_spacer indices are proportional to
            %distance along the x axis).
            if x_spacer(i) < x_arc_left
                y_spacer = [r_cond y_spacer_vertex1(i) y_spacer_vertex2(i) r_enclosure];
                for j = 1:4
                    fprintf(fileID, create_vertex, x_spacer(i), y_spacer(j));
                end
                edges = edges + Create_Edge(fileID,vertex,vertex + 1,vertex + 2,vertex + 3);
                vertex = vertex + 4;
                walls = [walls '"edge.',num2str(edges),'" "edge.',num2str(edges - 2),'"'];

                %Create Face
                fprintf(fileID, 'face split "face.%d" edges "edge.%d" "edge.%d" "edge.%d" \r\n',face_to_split,edges, edges - 1, edges - 2);
                face = face + 1;
                if  right_vertex - x_spacer(i) < (right_vertex - left_vertex)/2
                    face_elem = find(mesh_order(1,:) == face_to_split);
                    mesh_order(2,face_elem) = left_vertex;
                    temp = [face;x_spacer(i)];
                    mesh_order = [mesh_order temp];
                    face_to_split = face;
                    
                else
                    face_elem = find(mesh_order(1,:) == face_to_split);
                    mesh_order(2,face_elem) = x_spacer(i);
                    temp = [face;left_vertex];
                    mesh_order = [mesh_order temp];
                end
                left_vertex = x_spacer(i);
                walls = [walls '"edge.',num2str(edges + 1),'" "edge.',num2str(edges + 5),'"'];
                vertex = vertex + 4;
                edges = edges + 5;
                index_counter = index_counter + 1;
            else
                break
            end
        end
        
        %create arc zone vertices
        x_values = [x_arc_left x_arc_right];
        %calculating y_values depending on spacers
        y_values = [r_cond y_spacer_vertex1(spacer_number) y_spacer_vertex2(spacer_number) r_enclosure];
        length_arc_centre = y_spacer_vertex2(spacer_number) - y_spacer_vertex1(spacer_number); %length of the space between the centre vertices in the arc walls
        %print arc zone vertices
        for i = 1:length(x_values)
            for j = 1:length(y_values)
                fprintf(fileID, create_vertex, x_values(i), y_values(j));
            end
        end
        
        %Create left arc edges and split face with them
        edges  = edges + Create_Edge(fileID,vertex,vertex + 1,vertex + 2,vertex + 3);
        vertex = vertex + 4;
        
        %Create Face
        fprintf(fileID, 'face split "face.%d" edges "edge.%d" "edge.%d" "edge.%d" \r\n',face_to_split,edges, edges - 1, edges - 2);
        face = face + 1;
        if  right_vertex - x_arc_left < (right_vertex - left_vertex)/2
            face_elem = find(mesh_order(1,:) == face_to_split);
            mesh_order(2,face_elem) = left_vertex;
            temp = [face;x_arc_left];
            mesh_order = [mesh_order temp];
            face_to_split = face;
        else
            face_elem = find(mesh_order(1,:) == face_to_split);
            mesh_order(2,face_elem) = x_arc_left;
            temp = [face;left_vertex];
            mesh_order = [mesh_order temp];
        end
        left_vertex = x_arc_left;
        
        walls = [walls '"edge.',num2str(edges + 1),'" "edge.',num2str(edges + 5),'"'];
        edges = edges + 5;
        
        %Create right arc edges and split face with them
        edges  = edges + Create_Edge(fileID,vertex,vertex + 1,vertex + 2,vertex + 3);
        vertex = vertex + 4;
        
        %Create Face
        fprintf(fileID, 'face split "face.%d" edges "edge.%d" "edge.%d" "edge.%d" \r\n',face_to_split,edges,edges - 1,edges - 2);  % splits face with right arc zone wall
        face = face + 1;
        %If the arc area is larger than the face that was just split it
        %will change what face number the arc is
        if  right_vertex - x_arc_right < (right_vertex - left_vertex)/2
            face_elem = find(mesh_order(1,:) == face_to_split);
            mesh_order(2,face_elem) = left_vertex;
            temp = [face;x_arc_right];
            mesh_order = [mesh_order temp];
            arc_face = face - 1;
        else
            face_elem = find(mesh_order(1,:) == face_to_split);
            mesh_order(2,face_elem) = x_arc_right;
            temp = [face;left_vertex];
            mesh_order = [mesh_order temp];
            arc_face = face;
        end
        fprintf(fileID,' physics create "arc" ctype "FLUID" face "face.%d" \r\n ',arc_face);
        
        left_vertex = x_arc_right;
        
        walls = [walls '"edge.',num2str(edges + 1),'" "edge.',num2str(edges + 5),'"'];
        %When you split a face new vertices are created.(4 for this face) We
        %need to add 8 because the face is split twice. adding 8 vertices has
        %to be done after the edges are created because when we create the
        %edges we use the old vertices.
        vertex = vertex + 8;
        edges  = edges + 5;
        
        %Check that not all of the vertices are on the left side of the arc
        if index_counter <= N_spacers
            for i = index_counter:N_spacers
                y_spacer = [r_cond y_spacer_vertex1(i) y_spacer_vertex2(i) r_enclosure];
                for j = 1:4
                    fprintf(fileID, create_vertex, x_spacer(i), y_spacer(j));
                end
                edges = edges + Create_Edge(fileID,vertex,vertex + 1,vertex + 2,vertex + 3);
                vertex = vertex + 4;                
                walls = [walls '"edge.',num2str(edges),'" "edge.',num2str(edges - 2),'"'];
                fprintf(fileID, 'face split "face.%d" edges "edge.%d" "edge.%d" "edge.%d" \r\n',face_to_split,edges, edges - 1, edges - 2);
                face = face + 1;
                
                if  right_vertex - x_spacer(i) < (right_vertex - left_vertex)/2
                    face_elem = find(mesh_order(1,:) == face_to_split);
                    mesh_order(2,face_elem) = left_vertex;
                    temp = [face;x_spacer(i)];
                    mesh_order = [mesh_order temp];
                    face_to_split = face;
                else
                    face_elem = find(mesh_order(1,:) == face_to_split);
                    mesh_order(2,face_elem) = x_spacer(i);
                    temp = [face;left_vertex];
                    mesh_order = [mesh_order temp];
                end
                left_vertex = x_spacer(i);                
                walls = [walls '"edge.',num2str(edges + 1),'" "edge.',num2str(edges + 5),'"'];
                vertex = vertex + 4;
                edges = edges + 5;
                index_counter = index_counter + 1;
            end
        end
    end
end

%Sorting the faces from left to right

[Y,I] = sort(mesh_order(2,:));
mesh_order = mesh_order(:,I);

%Mesh all edges left to right
for i=1:length(mesh)
    fprintf(fileID, mesh{i});
end

%Mesh all faces from left to right
for i=1:length(mesh_order)
    fprintf(fileID,'face mesh "face.%d" size %d \r\n',mesh_order(1,i),coarse_mesh);
end