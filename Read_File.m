function [variable_names,values] = Read_File(filename)

%This function reads the input file and creates the variables

variable_names = [];
values = [];

fileID = fopen(sprintf('%s.txt',filename)); %update text file

%Get the first line of the file
tline = fgets(fileID);
%Read the file until the end
while ischar(tline)
    %Split the line on white spaces
    line = strsplit(tline);
    %Check if the line is empty. If it's not empty check if it starts with
    %'*'. Skip this line if either of these conditions is true
    if isempty(line{1}) == 1 || line{1}(1) == '*'
        tline = fgets(fileID);
        continue
    end   
    %Add the variable name to the array
    variable_names{end + 1} = line(1);
    %If the semi colon to add a comment is right after the number, we need
    %to remove it. Ex: Changing 0.012; to 0.012
    if line{2}(end) == ';'
        line{2} = line{2}(1:end - 1);
    end 
    %Add the value of the variable to the corresponding element in the
    %values array
    values(end + 1) = str2double(line(2));
    tline = fgets(fileID);
end

fclose(fileID); %Close the IAT_HVGIS file
