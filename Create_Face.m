function [new_face_count] = Create_Face(varargin)
%This function recieves a list of edge numbers as input and creates a face
%with that input. 

%The argument varargin allows for a variable amount of arguments to be
%supplied as input. By writing varargin{7}, you would be selecting the 7th
%argument that was input

edge = [];
fileID = varargin{1};
new_face_count = 1;

for i = 2:nargin
    edge = [edge '"edge.',num2str(varargin{i}),'" '];
end

fprintf(fileID,'face create wireframe %s \r\n',edge);