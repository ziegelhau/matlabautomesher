function [new_edge_count] = Create_Edge(varargin)
%This function recieves a list of vertex numbers as input and creates edges
%with that input

%The argument varargin allows for a variable amount of arguments to be
%supplied as input. By writing varargin{7}, you would be selecting the 7th
%argument that was input

vertex = [];
fileID = varargin{1};
new_edge_count = length(varargin) - 2;

for i = 2:nargin
    vertex = [vertex '"vertex.',num2str(varargin{i}),'" '];
end

fprintf(fileID,'edge create straight %s \r\n',vertex);
%Example of code being written
%fprintf(fileID, "vertex.1" "vertex.2" "vertex.3" "vertex.4" "vertex.5" "vertex.6" \r\n'); %edge 1 to 5